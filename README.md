# Kinky bot

Expands over the interact feature of the BoobBob.
Allowed 2+ people to be tagged in a single interaction.
Phrases taken from [BoobBot](https://github.com/BoobBot/BoobBot).

## How to use

To call the bot, start a message with `kinky`,
followed by a command, and tags.

## Commands

- interact: `kinky interact`
- pickup: `kinky pickup`
- insult: `kinky insult`
- kills: `kinky kills`

## License

This project is licensed under the AGPL-3.0 License - see the [LICENSE](LICENSE) file for details
