require('dotenv').config();
const Discord = require('discord.js');
const request = require('request');
const bot = new Discord.Client();
const TOKEN = process.env.TOKEN;

const IMGFLIPUSERNAME = process.env.IMGFLIPUSERNAME
const IMGFLIPPASSWORD = process.env.IMGFLIPPASSWORD

const RedditAPI = require('reddit-wrapper-v2')
  var redditConn = new RedditAPI({
  // Options for Reddit Wrapper
  username: process.env.REDDITUSER,
  password: process.env.REDDITPASS,
  app_id: process.env.REDDITAPIID,
  api_secret: process.env.REDDITAPISEC,
  user_agent: 'NodeJS',
  retry_on_wait: true,
  retry_on_server_error: 5,
  retry_delay: 1,
  logs: true
})

let subredditcommands = {
  "uncle": "/r/unclejokes/hot",
  "dad": "/r/dadjokes/hot",
  "jokes": "/r/jokes/hot",
  "3am": "/r/3amjokes/hot",
  "memes": "/r/memes/hot",
  "meirl": "/r/me_irl/hot",
  "gayirl": "/r/gay_irl/hot",
  "2meirl": "/r/2meirl4meirl/hot",
  "animeirl": "/r/anime_irl/hot",
  "okbh": "/r/okbuddyhetero/hot",
  "okbt": "/r/okbuddycatgirl/hot",
  "aww": "/r/aww/hot",
  "sgay": "/r/suddenlygay/hot",
  "traa": "/r/traaaaaaannnnnnnnnns/hot",
  "ameirl": "/r/absolutelynotme_irl/hot",
  "animemes": "/r/animemes/hot",
  "animenocon": "/r/animenocontext/hot",
  "grandma": "/r/forwardsfromgrandma/hot",
  "hmmm": "/r/hmmm/hot",
  "ibtinder": "/r/indianboysontinder/hot",
  "igtinder": "/r/indiangirlsontinder/hot",
  "linux": "/r/linuxmemes/hot",
  "math": "/r/mathjokes/hot",
  "onelines": "/r/oneliners/hot",
  "loop": "/r/perfectloops/hot",
  "pointlessgen": "/r/pointlesslygendered/hot",
  "pornm": "/r/pornmemes/hot",
  "progm": "/r/programmerhumor/hot",
  "puns": "/r/puns/hot",
  "wholesome": "/r/wholesomememes/hot",
  "woahdude": "/r/woahdude/hot",
  "ipfacebook": "/r/indianpeoplefacebook/hot",
  "eggirl": "/r/egg_irl/hot",
  "biirl": "/r/bi_irl/hot",
  "murder": "/r/murderedbywords/hot",
  "commun": "/r/unexpectedcommunism/hot",
  "insane": "/r/insanepeoplefacebook/hot",
  "dykwim": "/r/dontyouknowwhoiam/hot",
  "alyb": "/r/actlikeyoubelong/hot",
  "minf": "r/mildlyinfuriating/hot",
  "pubf": "r/publicfreakout/hot",
  "facep": "r/facepalm/hot",
  "idiotsf": "r/idiotsfightingthings/hot",
  "mademesm": "r/mademesmile/hot",
  "yttw": "r/whitepeopletwitter/hot",
  "vax": "r/vaxxhappened/hot",
  "bwa": "r/birdswitharms/hot",
  "wwwmd": "r/whatswrongwithmydog/hot"
}

const fs = require('fs')
let database = JSON.parse(fs.readFileSync('database.json'))
let jokesdb;

let memedb = JSON.parse(fs.readFileSync("memedb.json"))

function generateMeme(channel, template, text, preview) {

  let boxes = []
  for(let line in text) {
    boxes.push({"text": text[line]})
  }

  let data = {
    "username": IMGFLIPUSERNAME,
    "password": IMGFLIPPASSWORD,
    "template_id": template,
    "boxes": boxes
  }

  const options = {
    url: 'https://api.imgflip.com/caption_image',
    form: data
  };

  request.post(options, (err, res, body) => {
      if (err) {
          return console.log(err);
      }
      let response = JSON.parse(body)
      if (response.success) {
        channel.send((preview ? "This message will be deleted in 10 seconds.\n": "") + response.data.url).then((message) => {
          if (preview) {
            setTimeout(() => {
              message.delete()
            }, 10000)
          }
        })
      } else {
        channel.send(response.error_message)
      }
  });

}

function jokesUpdate(channel) {
  return new Promise((resolve, reject) => {
    if (jokesdb === undefined || (new Date() - jokesdb.lastupdated)/1000/3600 > 1) {
      jokesdb = {
        jokes: {}
      }
      let updatemessage;
      let updateRemaining = Object.keys(subredditcommands).length
      channel.send("Updating Jokes Databases. Please wait. Remaining: " + updateRemaining).then((message) => {
        updatemessage = message
      })
      for (let key in subredditcommands) {
        redditConn.api.get(subredditcommands[key], {
          limit: 100
        })
        .then((response) => {
          let responseCode = response[0]
          let responseData = response[1]
          if (responseCode === 200) {
            jokesdb.lastupdated = new Date()
            jokesdb.jokes[key] = responseData.data.children
          }
          if (updatemessage) {
            updateRemaining--
            if (updateRemaining > 0) {
              updatemessage.edit("Updating Jokes Databases. Please wait. Remaining: " + updateRemaining)
            }
            else {
              updatemessage.delete()
              resolve()
            }
          }
        })
        .catch((err) => {
          reject("api request failed: " + err)
        })
      }
    } else {
      resolve()
    }
  })
}

function databaseGet(kinktype, author, mentions) {
  if (mentions && author) {
    if (database[kinktype]) {
      if (database[kinktype][mentions.length-1] && (mentions.length <= 1 || Math.random() < 0.5)) {
        let topicDB = database[kinktype][mentions.length-1];
        let kinkString = topicDB[Math.floor(Math.random()*topicDB.length)];
        for (var i = 0; i <= mentions.length; i++) {
          kinkString = kinkString.replace(new RegExp("\\{"+i+"\\}", 'g'), i == 0 ? author : mentions[i-1]);
        }
        return kinkString;
      } else if (database[kinktype][mentions.length-2]) {
        let topicDB = database[kinktype][mentions.length-2];
        let kinkString = topicDB[Math.floor(Math.random()*topicDB.length)];
        for (var i = 0; i < mentions.length; i++) {
          kinkString = kinkString.replace(new RegExp("\\{"+i+"\\}", 'g'), mentions[i]);
        }
        return kinkString;
      } else {
        return "Too many or few mentions (" + mentions.length + " but max is " + (database[kinktype].length + 1) + " ). " + author + " is such a whore."
      }
    } else {
      return "Invalid Usage. " + kinktype + " not avialable. Even I'm not this weird."
     }
  } else {
    return "`mentions or author undefined`"
  }
}

bot.login(TOKEN);

bot.on('ready', () => {
  console.info(`Logged in as ${bot.user.tag}!`);
});

bot.on('message', msg => {
  if (msg.author.id !== bot.user.id) {
    let args = msg.content.toLowerCase().split(/ +/);
    if (args.length > 1 && args[0] === "kinky") {
      setTimeout(() => {
        msg.delete()
      }, 3000)
      if (args[1] === "help") {
        let helpstring = "Usage: `kinky [command] [tags] [--Text for memes]`\nFollowing reddit commands are avialable: `random` "
        for (let key in subredditcommands) {
          helpstring+= "`"+key+"`  "
        }
        helpstring+= "\nFollowing commands can be used with tags: "
        for (let key in database) {
          helpstring+= "`"+key+"`  "
        }
        msg.channel.send(helpstring)
        helpstring= "\nFollowing commands can be used to generate memes: "
        for (let key in memedb) {
          helpstring+= "`"+key+"`  "
        }
        helpstring+= "\nExample: `kinky distractedbf --kinky --Me --all other bots`"

        msg.channel.send(helpstring)
      } else if(args[1] === "random") {
        let keys = Object.keys(subredditcommands)
        let sub = keys[Math.floor(Math.random()*keys.length)]
        jokesUpdate(msg.channel).then(() => {
          let joke = jokesdb.jokes[sub][Math.floor(Math.random() * jokesdb.jokes[sub].length)]
          msg.channel.send(joke.data.title + "\n|| " + joke.data.selftext + "\n" + joke.data.url + " ||")
        }).catch((err) => {
          console.error(err);
        })
      } else if (Object.keys(subredditcommands).find(key => args[1]==key)) {
        jokesUpdate(msg.channel).then(() => {
          let joke = jokesdb.jokes[args[1]][Math.floor(Math.random() * jokesdb.jokes[args[1]].length)]
          msg.channel.send(joke.data.title + "\n|| " + joke.data.selftext + "\n" + joke.data.url + " ||")
        }).catch((err) => {
          console.error(err);
        })
      } else if (Object.keys(memedb).find(key => args[1]==key)) {
        let textboxes = msg.content.split("--").splice(1);
        generateMeme(msg.channel, memedb[args[1]], textboxes)
      } else if(args[1] == "memepreview") {
        if (args[2] && Object.keys(memedb).find(key => args[2]==key)) {
          let demoboxes = []
          for (var i = 1; i <= 10; i++) {
            demoboxes.push("Text " + i)
          }
          let message = generateMeme(msg.channel, memedb[args[2]], demoboxes, true)
        } else {
          msg.channel.send("Meme Not Found")
        }
      } else if(args[1] === "roles") {
        for (role in rolesdb) {
          ((role) => {
            let embed = new Discord.RichEmbed()
            .setTitle(role)
            .setDescription(rolesdb[role].message)
            msg.channel.send(embed).then(async (rolemessage) => {
              try {
                for (reaction in rolesdb[role].roles) {
                  await rolemessage.react(reaction)
                }
              } catch (e) {
                console.error(e)
              }
            })
          })(role)
        }
      } else {
        msg.channel.send(databaseGet(args[1], msg.author, msg.mentions.users.array()))
      }
    }
  }
});

bot.on('messageReactionAdd', async (reaction, user) => {
	if (reaction.partial) {
		try {
			await reaction.fetch();
		} catch (error) {
			console.error('Something went wrong when fetching the message: ', error);
			return;
		}
	}
});

bot.on('messageReactionRemove', async (reaction, user) => {
  if (reaction.partial) {
		try {
			await reaction.fetch();
		} catch (error) {
			console.error('Something went wrong when fetching the message: ', error);
			return;
		}
	}
});

setInterval(syncRoles, 10000)
